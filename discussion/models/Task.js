//Create a Schema, model for the task collection, and make sure to export

const mongoose = require('mongoose');

const taskBlueprint = new mongoose.Schema({
	name:{
		type: String,
		required: [true, 'Task Name is Required!']
	},
	status:{
		type: String,
		default: 'pending'
	}
});

//create moedl using schema
module.exports = mongoose.model("Task", taskBlueprint);