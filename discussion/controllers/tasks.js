//[SECTION] Dependencies and Modules
const Task = require('../models/Task');

//[SECTION] Functionalities

	//Create New Task
		module.exports.createTask = (clientInput) => {
			let taskName = clientInput.name;
			let newTask = new Task({
				name: taskName
			});
			return newTask.save().then((task, error) => {
				if (error) {
					return 'Saving New Task Failed!';
				} else {
					return 'A New Task Successfully Created!';
				}
			})
		};

	//Retrieve All Tasks
		module.exports.getAllTasks = () => {
			return Task.find({}).then(searchResult => {
				return searchResult;
			})
		};

		//Retreive a Single Task
		module.exports.getTask = (data) => {
			return Task.findById(data).then(result => { 
				return result;
			})
		};

	//Update a User
		module.exports.updateTask = (taskId, updated) => {

			let tStat = updated.status;

			return Task.findById(taskId).then((foundTask, error) => {
			
				if (foundTask) {
					foundTask.status = tStat;
					return foundTask.save().then((updatedTask, saveError) => {
						if (updatedTask) {
							return updatedTask;
						} else {
							return false;
						}
					})
				} else {
					return 'No Task Found'
				}
			});
		};

	//Delete a Task
		module.exports.deleteTask = (taskId) => {
			return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
				if (removedTask) {
					return `${removedTask} is completely deleted.` 
				} else {
					return 'No Tasks were removed!'
				}
			}) 
		};

