//[SECTION] Dependencied and Modules
	const User = require('../models/User');
	const bcrypt = require('bcrypt');

//[SECTION] User functionalities
	
	//Create a new user
	module.exports.registerUser = (requestBody) => {
		let fName = requestBody.firstName;
		let lName = requestBody.lastName;
		let email = requestBody.email;
		let passW = requestBody.password;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10)
		});

		return newUser.save().then((user, error) => {
			if (user) {
				console.log(user);
				return user;
			} else {
				return 'Failed to create user :(.'
			}
		})
	};

	//Retrieve all Users
	module.exports.getAllUsers = () => {
		return User.find({}).then(resultOfQuery => {
			return resultOfQuery;
		})
	};

	//Retreive a Single User
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			result.password = "******"; 
			return result;
		})
	};


	//Delete a user
	module.exports.deleteUser = (userId) => {
		return User.findByIdAndRemove(userId).then((removedUser, err) => {
			if (removedUser) {
				console.log(removedUser);
				return `${removedUser} is Deleted Successfully!` 
			} else {
				return 'No accounts were removed!'
			}
		}) 
	};


	//Update a User
	module.exports.updateUser = (userId, newContent) => {
	//user? using the Id property
	// details about the user
		let fName = newContent.firstName;
		let lName = newContent.lastName;		

		return User.findById(userId).then((foundUser, error) => {
			//Create a control structure that will determine the response according to the state of the promise
			if (foundUser) {
				foundUser.firstName = fName;
				foundUser.lastName = lName;
				return foundUser.save().then((updatedUser, saveErr) => {
					if (saveErr) {
						return false;
					} else {
						return updatedUser;
					}
				})
			} else {
				return 'No User Found'
			}
		});
	};