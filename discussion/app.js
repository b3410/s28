//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const taskRoutes = require('./routes/tasks');
	const userRoutes = require('./routes/users');

//[SECTION] Environment Variables
	dotenv.config();
	let secret = process.env.CONNECTION_STRING;

//[SECTION] Server Setup
	const server = express();
	server.use(express.json()); //middleware
	const port = process.env.PORT;

//[SECTION] Database Connection
		mongoose.connect(secret);
		let db = mongoose.connection;
		db.on('open', () => console.log('Now Connected to MongoDb Atlas'));
	
//[SECTION] Server Routes
	server.use('/tasks', taskRoutes);
	server.use('/users', userRoutes);

//[SECTION] Server Response
	server.listen(port, () => {
		console.log(`Server is running on port: ${port}`)
	});

	server.get('/', (req, res) => {
		res.send('Welcome to my app!')
	});
