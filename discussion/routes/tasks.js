//[SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks');

//[SECTION] Routing Component
	const route = express.Router();

//[SECTION] Task Routes
	//Create Task
		route.post('/', (req, res) => {
			//execute the createTask() from the controller
			let taskInfo = req.body;
			controller.createTask(taskInfo).then(result => res.send(result))
		});
	
	//Retrieve All tasks
		route.get('/', (req,res) => {
			controller.getAllTasks().then(result => {
				res.send(result);
		})
		});

	//Retrieve Task
		route.get('/:id', (req, res) => {
			let taskId = req.params.id;
			controller.getTask(taskId).then(end => 
				res.send(end));
		});

	//Update Task details
		route.put('/:id', (req, res) => {
			let id = req.params.id;
			let bod = req.body;
			controller.updateTask(id, bod).then(result => {
				res.send(result)
			})
		});

	//Delete Task
		route.delete('/:id' , (req, res) => {
			let taskId = req.params.id;
			controller.deleteTask(taskId).then(result => {
				res.send(result);
			})
		});



//[SECTION] Expose Route System
	module.exports = route;

